const express = require('express')
const pino = require('pino')
const app = express()
const port = 8000

const logger = pino()

const apm = require('elastic-apm-node').start({
  serviceName: 'testing',
  serverUrl: 'http://localhost:8200',
  environment: 'production',
  captureBody: 'all',
  logLevel: 'trace',
  logger: pino()
})
app.use(apm.middleware.connect())

app.get('/', (req, res) => {
  // apm.startTransaction('33dfe7239537d856')
  apm.logger.info({ asdasd: 'asdasdasd' })
  console.log('asdasdasdasd');
  res.send('Hello World!')
  // apm.endTransaction()
})

app.get('/error', (req, res) => {
  var err = new Error('Ups, something broke!')
  apm.captureError(err)
  res.send('error loh')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
